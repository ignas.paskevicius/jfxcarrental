package com.ignas.carrental.repositories;

import com.ignas.carrental.model.Person;
import com.ignas.carrental.model.RentEvents;
import com.ignas.carrental.utils.DatabaseUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class PersonRepository {

    /*
    * Selects all data from persons table
    */
    public List<Person> findAll(){
        List<Person> personList = new ArrayList<>();

        Session session = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            personList = session.createQuery("from Person", Person.class).list();
            System.out.println("\nSelecting all persons from DB...");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            assert session != null;
            session.close();
        }

        return personList;
    }

    public void update(Person personToUpdate){

        Transaction transaction;
        Session session = null;

        System.out.println("personToUpdate ID: " + personToUpdate.getPersonId());

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.merge(personToUpdate);
            transaction.commit();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            assert session != null;
            session.close();
        }
    }

    public void delete(Person personToDelete){
        Transaction transaction = null;
        Session session = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(personToDelete);
            transaction.commit();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            assert session != null;
            session.close();
        }
    }

    public Person loginAuthorization(String userName, String password){
        Person person = new Person();
        List<Person> personList;

        String sql = "FROM Person p WHERE p.userName = :userName AND p.password = :password";

        Session session = null;
        Query query = null;
        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            query = session.createQuery(sql);
            query.setParameter("userName", userName);
            query.setParameter("password", password);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            assert query != null;
            personList = query.list();
            personList.forEach(p -> {
                person.setPersonId(p.getPersonId());
                person.setUserName(p.getUserName());
                person.setPassword(p.getPassword());
                person.setFirstName(p.getFirstName());
                person.setLastName(p.getLastName());
                person.setEmail(p.getEmail());
                person.setDateOfBirth(p.getDateOfBirth());
                person.setPersonRents(p.getPersonRents());
            });
            session.close();
        }

        return person;
    }
}
