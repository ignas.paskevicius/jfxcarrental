package com.ignas.carrental.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer carId;

    @Column(name = "Brand")
    private String brand;

    @Column(name = "Model")
    private String model;

    @Column(name = "Make")
    private String make;

    @Column(name = "Type")
    private String type;

    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER)
    private Set<RentEvents> carRents = new HashSet<>();

    /*@ManyToMany
    @JoinTable(
            name = "rentmanagement",
            joinColumns = { @JoinColumn(name = "CarId")},
            inverseJoinColumns = {@JoinColumn(name = "PersonId")}
    )
    private Set<Person> persons = new HashSet<>();*/

    public Set<RentEvents> getCarRents() {
        return carRents;
    }

    public void setCarRents(Set<RentEvents> carRents) {
        this.carRents = carRents;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    @Override
    public String toString() {
        return "Car{" +
                "carId=" + carId +
                ", brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", make='" + make + '\'' +
                ", type='" + type + '\'' +
                //", carRents=" + carRents +
                '}';
    }
}
