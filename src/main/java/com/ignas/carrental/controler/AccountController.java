package com.ignas.carrental.controler;

import com.ignas.carrental.model.Person;
import com.ignas.carrental.repositories.PersonRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class AccountController {
    private Person selectedPerson;

    @FXML
    private Label labelNote;

    @FXML
    private TextField textFirstName;

    @FXML
    private TextField textLastName;

    @FXML
    private TextField textEmail;

    @FXML
    private DatePicker dateOfBirth;

    @FXML
    private TextField textPassword;

    @FXML
    private TextField textNewPassword;

    @FXML
    private TextField textRepeatedPassword;

    @FXML
    private TextField buttonBack;

    public void initPerson(Person person){
        selectedPerson = person;
        textFirstName.setText(selectedPerson.getFirstName());
        textLastName.setText(selectedPerson.getLastName());
        textEmail.setText(selectedPerson.getEmail());
        dateOfBirth.setValue(selectedPerson.getDateOfBirth());
        System.out.println("Selected person: " + selectedPerson + " " + selectedPerson.getPersonId());
    }

    public void back(ActionEvent event) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader();
        try{
            loader.setLocation(getClass().getResource("/mainScene.fxml"));
            root = loader.load();
        }catch (Exception e){
            e.printStackTrace();
        }
        assert root != null;
        Scene scene = new Scene(root);
        MainController controller = loader.getController();
        controller.initPerson(selectedPerson);
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void changePassword(ActionEvent event) {
        if(textPassword.getText() != null && textPassword.getText().equals(selectedPerson.getPassword())){
            if(textNewPassword.getText().equals(textRepeatedPassword.getText()));
            selectedPerson.setPassword(textNewPassword.getText());
            PersonRepository personRepository = new PersonRepository();
            personRepository.update(selectedPerson);
            System.out.println("Successful change!");
        }else {
            labelNote.setText("Password can not be empty!");
        }
    }

    public void savePersonChanges() {
        selectedPerson.setFirstName(textFirstName.getText());
        selectedPerson.setLastName(textLastName.getText());
        selectedPerson.setEmail(textEmail.getText());
        selectedPerson.setDateOfBirth(dateOfBirth.getValue());

        System.out.println("\nSelected person before update: " + selectedPerson + " " + selectedPerson.getPersonId());

        PersonRepository personRepository = new PersonRepository();
        personRepository.update(selectedPerson);
    }

    public void deletePerson(ActionEvent event) {
        System.out.println("Person to delete: " + selectedPerson.getPersonId());
        PersonRepository personRepository = new PersonRepository();
        personRepository.delete(selectedPerson);

        Parent root = null;

        try{
            root = FXMLLoader.load(getClass().getResource("/loginScene.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
