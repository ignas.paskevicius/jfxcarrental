package com.ignas.carrental.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "rentevents")
public class RentEvents {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer rentId;

    @Column(name = "CarId")
    private Integer carId;

    @Column(name = "PersonId")
    private Integer personId;

    @Column(name = "StartDate")
    private LocalDate startDate;

    @Column(name = "EndDate")
    private LocalDate endDate;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "carId")
    private Car car;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "personId")
    private Person person;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getRentId() {
        return rentId;
    }

    public void setRentId(Integer rentId) {
        this.rentId = rentId;
    }

    public Integer getCarId() {
        return carId;
    }

    public void setCarId(Integer carId) {
        this.carId = carId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "RentManagement{ " +
                car.getBrand() + " " +
                car.getModel() + " " +
                carId + " " +
                startDate + " - " +
                endDate + " }";
    }

    /*@Override
    public String toString() {
        return "RentManagement{" +
                "rentId=" + rentId +
                ", carId=" + carId +
                ", personId=" + personId +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                '}';
    }*/
}
