package com.ignas.carrental;

import com.ignas.carrental.model.Person;
import com.ignas.carrental.repositories.PersonRepository;
import com.ignas.carrental.utils.DatabaseUtils;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        PersonRepository personRepository = new PersonRepository();

        for (Person p : personRepository.findAll()) {
            System.out.println(p);
        }

        launch(args);
        DatabaseUtils.shutdown();
    }

    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("Car Rental");
        Parent root = null;

        try{
            root = FXMLLoader.load(getClass().getResource("/loginScene.fxml"));

        }catch (Exception e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
