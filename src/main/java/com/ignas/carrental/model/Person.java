package com.ignas.carrental.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "persons")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer personId;

    @Column(name = "FirstName")
    private String firstName;

    @Column(name = "LastName")
    private String lastName;

    @Column(name = "Email")
    private String email;

    @Column(name = "DateOfBirth")
    private LocalDate dateOfBirth;

    @Column(name = "UserName")
    private String userName;

    @Column(name = "Password")
    private String password;

    /*@ManyToMany(mappedBy = "persons")
    private Set<Car> cars = new HashSet<>();*/

    @OneToMany(mappedBy = "person", fetch = FetchType.EAGER)
    private Set<RentEvents> personRents = new HashSet<>();

    public Set<RentEvents> getPersonRents() {
        return personRents;
    }

    public void setPersonRents(Set<RentEvents> personRents) {
        this.personRents = personRents;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = Objects.requireNonNull(userName);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) { this.password = password; }

    @Override
    public String toString() {
        return  firstName + ' ' +
                lastName + ' ' +
                email + ' ' +
                userName + ' ' +
                password + ' ' +
                dateOfBirth + ' ' +
                personRents;
    }
}
