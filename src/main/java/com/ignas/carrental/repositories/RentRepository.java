package com.ignas.carrental.repositories;

import com.ignas.carrental.model.Car;
import com.ignas.carrental.model.Person;
import com.ignas.carrental.model.RentEvents;
import com.ignas.carrental.utils.DatabaseUtils;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class RentRepository {
    private Car selectedCar;

    public List<RentEvents> findAll(){
        List<RentEvents> rentList = new ArrayList<>();

        Session session;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            rentList = session.createQuery("from RentEvents", RentEvents.class).list();
        }catch (Exception e){
            e.printStackTrace();
        }
        return rentList;
    }

    public List<RentEvents> personRents(int id){
        List<RentEvents> personRentsList;

        String sql = "FROM RentEvents r WHERE r.personId = :id";
        Session session = null;
        Query query = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            query = session.createQuery(sql);
            query.setParameter("id", id);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            assert query != null;
            personRentsList = query.list();
            session.close();
        }
        return personRentsList;
    }

    public void delete(RentEvents rentToDelete){
        Transaction transaction = null;
        Session session = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(rentToDelete);
            transaction.commit();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            assert session != null;
            session.close();
        }
    }

    public void update(RentEvents rentToUpdate){

        Transaction transaction;
        Session session = null;

        System.out.println("rentToUpdate: " + rentToUpdate);

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(rentToUpdate);
            transaction.commit();
        }catch (Exception ex){
            ex.printStackTrace();
        }finally {
            assert session != null;
            session.close();
        }
    }

}
