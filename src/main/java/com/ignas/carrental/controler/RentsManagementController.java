package com.ignas.carrental.controler;

import com.ignas.carrental.model.Car;
import com.ignas.carrental.model.Person;
import com.ignas.carrental.model.RentEvents;
import com.ignas.carrental.repositories.CarRepository;
import com.ignas.carrental.repositories.RentRepository;
import com.ignas.carrental.utils.DatabaseUtils;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class RentsManagementController implements Initializable {
    ObservableList<RentEvents> rentsList = FXCollections.observableArrayList();

    private Person selectedPerson;
    private List<String> brandModels;
    private List<Car> carList;
    private IntegerProperty index = new SimpleIntegerProperty();

    public void initPerson(Person person){
        selectedPerson = person;
    }

    @FXML
    private TableView<RentEvents> allPersonRentsTable;
    @FXML
    private TableColumn<RentEvents, String> carBrand;
    @FXML
    private TableColumn<RentEvents, String> carModel;
    @FXML
    private TableColumn<RentEvents, LocalDate> rentStartDate;
    @FXML
    private TableColumn<RentEvents, LocalDate> rentEndDate;
    @FXML
    private ComboBox<String> comboBoxBrand;
    @FXML
    private ComboBox<String> comboBoxModel;
    @FXML
    private DatePicker editStartDate;
    @FXML
    private DatePicker editEndDate;

    public void allPersonRents(){
        RentRepository rentRepository = new RentRepository();
        rentsList.removeAll(rentsList);

        for(RentEvents r : rentRepository.personRents(selectedPerson.getPersonId())){
            rentsList.add(r);
            System.out.println(r);
        }
        allPersonRentsTable.setItems(rentsList);
    }

    public void back(ActionEvent event) {
        Parent root = null;
        FXMLLoader loader = new FXMLLoader();

        try{
            loader.setLocation(getClass().getResource("/mainScene.fxml"));
            root = loader.load();
        }catch (Exception e){
            e.printStackTrace();
        }
        assert root != null;
        Scene scene = new Scene(root);
        MainController controller = loader.getController();
        controller.initPerson(selectedPerson);
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }

    public void deleteRentEvent(ActionEvent event) {
        RentRepository rentRepository = new RentRepository();

        if(index.get() > -1){
            rentRepository.delete(rentsList.get(index.get()));
            rentsList.remove(index.get());
            allPersonRentsTable.getSelectionModel().clearSelection();
            allPersonRentsTable.setItems(rentsList);
        }
    }

    public void updateRentEvent(ActionEvent event) {
        RentRepository rentRepository = new RentRepository();
        CarRepository carRepository = new CarRepository();

        RentEvents rentToUpdate = rentsList.get(index.get());
        rentToUpdate.setCarId(carRepository.findByCarModel(comboBoxModel.getValue()).getCarId());
        rentToUpdate.setStartDate(editStartDate.getValue());
        rentToUpdate.setEndDate(editEndDate.getValue());

        if(index.get() > -1){
            rentRepository.update(rentToUpdate);
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb){
        //set up the columns in the table
        carBrand.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getCar().getBrand()));
        carModel.setCellValueFactory(param -> new SimpleStringProperty(param.getValue().getCar().getModel()));
        rentStartDate.setCellValueFactory(new PropertyValueFactory<>("startDate"));
        rentEndDate.setCellValueFactory(new PropertyValueFactory<>("endDate"));

        //listener to change car models by selected car brand
        comboBoxBrand.getSelectionModel().selectedItemProperty().addListener(((observableValue, oldValue, newValue) -> {
            comboBoxModel.getItems().clear();
            CarRepository carRepository = new CarRepository();
            carList = carRepository.findByCarBrand(comboBoxBrand.getValue());
            brandModels = carList.stream().map(Car::getModel).collect(Collectors.toList());
            comboBoxModel.getItems().clear();
            comboBoxModel.getItems().addAll(brandModels);
            comboBoxModel.getSelectionModel().select(0);
        }));

        //get index of a row when clicking and set EDIT RENT fields
        allPersonRentsTable.getSelectionModel().selectedItemProperty().addListener((observableValue, oldValue, newValue) -> {
            comboBoxBrand.getItems().clear();
            CarRepository carRepository = new CarRepository();
            index.set(rentsList.indexOf(newValue));
            System.out.println("Index is: " + rentsList.indexOf(newValue));
            if (index.get() > -1) {
                carRepository.findAll().stream()
                        .map(Car::getBrand)
                        .distinct()
                        .forEach(c -> {
                            comboBoxBrand.getItems().add(c);
                        });
            }
            comboBoxBrand.setValue(rentsList.get(index.get()).getCar().getBrand());
            editStartDate.setValue(rentsList.get(index.get()).getStartDate());
            editEndDate.setValue(rentsList.get(index.get()).getEndDate());
        });
    }
}
