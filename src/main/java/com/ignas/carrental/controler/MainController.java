package com.ignas.carrental.controler;

import com.ignas.carrental.model.Person;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.event.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private Person selectedPerson;

    @FXML
    private Label labelLoggedInPerson;

    public void initPerson(Person person){
        selectedPerson = person;
        labelLoggedInPerson.setText(selectedPerson.getFirstName() + " " + selectedPerson.getLastName());
    }

    public void logOut(ActionEvent event) {
        Parent root = null;

        try{
            root = FXMLLoader.load(getClass().getResource("/loginScene.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void accountSettings(ActionEvent event){
        Parent root = null;
        FXMLLoader loader = new FXMLLoader();
        try{
            loader.setLocation(getClass().getResource("/accountSettingsScene.fxml"));
            root = loader.load();
        }catch (Exception e){
            e.printStackTrace();
        }
        assert root != null;
        Scene scene = new Scene(root);
        AccountController controller = loader.getController();
        controller.initPerson(selectedPerson);
        Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void manageRents(ActionEvent event){
        Parent root = null;
        FXMLLoader loader = new FXMLLoader();
        try{
            loader.setLocation(getClass().getResource("/rentsManagementScene.fxml"));
            root = loader.load();
        }catch (Exception e){
            e.printStackTrace();
        }
        assert root != null;
        Scene scene = new Scene(root);
        RentsManagementController controller = loader.getController();
        controller.initPerson(selectedPerson);
        Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }
}
