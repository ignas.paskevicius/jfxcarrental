package com.ignas.carrental.repositories;

import com.ignas.carrental.model.Car;
import com.ignas.carrental.model.RentEvents;
import com.ignas.carrental.utils.DatabaseUtils;
import com.mysql.cj.protocol.x.Notice;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.xml.crypto.Data;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class CarRepository {
    public List<Car> findAll(){
        List<Car> carList = new ArrayList<>();

        Session session;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            carList = session.createQuery("from Car", Car.class).list();
        }catch (Exception e){
            e.printStackTrace();
        }
        return carList;
    }
    public List<Car> findByCarBrand(String carBrand){
        List<Car> carList;

        String sql = "FROM Car c WHERE c.brand = :carBrand";
        Session session = null;
        Query query = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            query = session.createQuery(sql);
            query.setParameter("carBrand", carBrand);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            assert query != null;
            carList = query.list();
            session.close();
        }
        return carList;
    }

    public Car findByCarModel(String carModel){
        List<Car> carList;
        Car car = new Car();

        String sql = "FROM Car c WHERE c.model = :carModel";
        Session session = null;
        Query query = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            query = session.createQuery(sql);
            query.setParameter("carModel", carModel);
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            carList = query.list();
            car = carList.get(0);
            assert session != null;
            session.close();
        }
        return car;
    }

    public List<Car> availableCarsForRent(LocalDate startDate, LocalDate endDate){
        List<Car> carsList = new ArrayList<>();

        String sql = "";
        Session session = null;
        Query query = null;

        try{
            session = DatabaseUtils.getSessionFactory().openSession();
            query = session.createQuery(sql);

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            carsList = query.list();
            assert session != null;
            session.close();
        }

        return carsList;
    }
}
