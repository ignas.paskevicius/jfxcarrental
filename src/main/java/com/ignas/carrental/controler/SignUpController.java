package com.ignas.carrental.controler;

import com.ignas.carrental.model.Person;
import com.ignas.carrental.utils.DatabaseUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.awt.*;

public class SignUpController {
    @FXML
    private TextField userName;

    @FXML
    private TextField firstName;

    @FXML
    private TextField lastName;

    @FXML
    private TextField email;

    @FXML
    private PasswordField password;

    @FXML
    private DatePicker dateOfBirth;

    public void back(ActionEvent event) {
        Parent root = null;
        try{
            root = FXMLLoader.load(getClass().getResource("/loginScene.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }


    public void signUp(ActionEvent event) {
        Person person = new Person();
        person.setUserName(userName.getText());
        person.setFirstName(firstName.getText());
        person.setLastName(lastName.getText());
        person.setEmail(email.getText());
        person.setPassword(password.getText());
        person.setDateOfBirth(dateOfBirth.getValue());

        Transaction transaction;
        Session session = null;
        Parent root = null;

        try {
            session = DatabaseUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(person);
            transaction.commit();
            root = FXMLLoader.load(getClass().getResource("/loginScene.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        Scene scene = new Scene(root);
        Stage stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show();
    }
}
