package com.ignas.carrental.controler;

import com.ignas.carrental.model.Person;
import com.ignas.carrental.repositories.PersonRepository;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.hibernate.Session;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginController implements Initializable {
    @FXML
    private Label labelLoginStatus;

    @FXML
    private TextField txtUserName;

    @FXML
    private TextField txtPassword;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*signUp.setOnAction(event -> System.out.println("Sign Up Click!"));
        buttonLogin.setOnAction(event -> System.out.println("Login Click!"));*/
    }

    public void login(ActionEvent event) {
        PersonRepository personRepository = new PersonRepository();
        Person person = personRepository.loginAuthorization(txtUserName.getText(), txtPassword.getText());
        if(txtUserName.getText().equals(person.getUserName()) && txtPassword.getText().equals(person.getPassword())){
            Parent root = null;
            FXMLLoader loader = new FXMLLoader();
            try{
                loader.setLocation(getClass().getResource("/mainScene.fxml"));
                root = loader.load();
            }catch (Exception e){
                e.printStackTrace();
            }
            assert root != null;
            Scene scene = new Scene(root);
            MainController controller = loader.getController();
            controller.initPerson(person);
            Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
            primaryStage.setScene(scene);
            primaryStage.show();
        }else{
            labelLoginStatus.setText("Login failed!");
        }
    }

    public void signUpAccount(ActionEvent event) {
        System.out.println("Hello");
        Parent root = null;

        try{
            root = FXMLLoader.load(getClass().getResource("/signUpScene.fxml"));
        }catch (Exception e){
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        Stage primaryStage = (Stage)((Node)event.getSource()).getScene().getWindow();
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
