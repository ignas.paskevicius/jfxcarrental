package com.ignas.carrental.controler;

import com.ignas.carrental.model.Car;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

public class CarRentController implements Initializable {

    @FXML
    private DatePicker rentStartDate;
    @FXML
    private DatePicker rentEndDate;
    @FXML
    private TableView<Car> allAvailableCarsTable;
    @FXML
    private TableColumn<Car, String> carBrand;
    @FXML
    private TableColumn<Car, String> carModel;
    @FXML
    private TableColumn<Car, String> carType;
    @FXML
    private TableColumn<Car, LocalDate> carMake;

    public void searchCarsForRent(ActionEvent event) {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        carBrand.setCellValueFactory(new PropertyValueFactory<>("brand"));
        carModel.setCellValueFactory(new PropertyValueFactory<>("model"));
        carType.setCellValueFactory(new PropertyValueFactory<>("type"));
        carMake.setCellValueFactory(new PropertyValueFactory<>("make"));
    }
}
